# Masquerading Onions

This project is an attempt to implement a simple user-space network stack that
can handle TCP and UDP state such that it is possible to forward the traffic
into the Tor network.

## Running

Create the `onion0` interface using (remember to replace `ahf` with
your own username):

    $ sudo ip tuntap add name onion0 mode tun user ahf
    $ sudo ip link set onion0 up
    $ sudo ip addr add 10.42.42.1/24 dev onion0
