use std::collections::BTreeMap;

use std::os::unix::io::AsRawFd;

use simple_logger::SimpleLogger;

extern crate clap;
use clap::{App, Arg};

extern crate log;
use log::debug;

extern crate smoltcp;
use smoltcp::iface::{InterfaceBuilder, Routes};
use smoltcp::phy::{wait as phy_wait, Medium, TunTapInterface};
use smoltcp::socket::{Socket, TcpSocket, TcpSocketBuffer};
use smoltcp::time::Instant;
use smoltcp::wire::{IpAddress, IpCidr, IpProtocol, Ipv4Address, Ipv4Packet, TcpPacket};

mod net;
use net::*;

// Our version.
const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    // Setup our logger.
    SimpleLogger::new().env().init().unwrap();

    // We use Clap for handling arguments to our process.
    let matches = App::new("Onionmasq")
        .bin_name("onionmasq")
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .version(VERSION)
        .author(env!("CARGO_PKG_AUTHORS"))
        .arg(
            Arg::with_name("interface")
                .required(true)
                .takes_value(true)
                .index(1)
                .default_value("onion0")
                .help("Specify the interface to listen on"),
        )
        .get_matches();

    // Starting up.
    log::info!("Onionmasq {} starting", VERSION);

    // Which interface do we use?
    let interface_name = matches.value_of("interface").unwrap();

    // Create our device.
    let device = TunTapInterface::new(&interface_name, Medium::Ip).unwrap();
    let device_fd = device.as_raw_fd();

    // Create our virtual device.
    let device = VirtualDevice::new(device);

    // Create our interface.
    let mut routes = Routes::new(BTreeMap::new());
    routes
        .add_default_ipv4_route(Ipv4Address::new(0, 0, 0, 1))
        .unwrap();
    let interface_builder = InterfaceBuilder::new(device, Vec::new())
        .ip_addrs([IpCidr::new(IpAddress::v4(0, 0, 0, 1), 0)])
        .routes(routes)
        .any_ip(true);

    // Finalize our interface.
    let mut interface = interface_builder.finalize();

    // Our DNS Manager.
    let mut dns_manager = DnsManager::new();
    dns_manager.register_udp_listener(IpAddress::v4(10, 42, 42, 2), 53, &mut interface);

    loop {
        let timestamp = Instant::now();

        // The first poll we do queue packets in the receive queue.
        interface
            .device()
            .set_poll_mode(VirtualDevicePollMode::QueuePackets);

        match interface.poll(timestamp) {
            Ok(_) => {}
            Err(error) => {
                debug!("Poll error: {}", error)
            }
        }

        // Handle incoming packet.
        for packet in interface.device().incoming_packets().iter() {
            match Ipv4Packet::new_checked(&packet[..]) {
                Ok(packet) => {
                    match packet.protocol() {
                        IpProtocol::Tcp => match TcpPacket::new_checked(packet.payload()) {
                            Ok(tcp_packet) => {
                                if tcp_packet.syn() && !tcp_packet.ack() {
                                    let src_addr = packet.src_addr();
                                    let src_port = tcp_packet.src_port();
                                    let dst_addr = packet.dst_addr();
                                    let dst_port = tcp_packet.dst_port();

                                    log::info!(
                                        "New incoming TCP connection: {}:{} -> {}:{}",
                                        src_addr,
                                        src_port,
                                        dst_addr,
                                        dst_port
                                    );

                                    let socket = TcpSocket::new(
                                        TcpSocketBuffer::new(vec![0; 4096]),
                                        TcpSocketBuffer::new(vec![0; 4096]),
                                    );

                                    let handle = interface.add_socket(socket);
                                    let socket = interface.get_socket::<TcpSocket>(handle);

                                    assert!(!socket.is_open());
                                    assert!(!socket.is_active());

                                    socket.listen((dst_addr, dst_port)).unwrap();
                                }
                            }

                            Err(err) => {
                                log::error!("Unable to create TCP packet: {}", err);
                            }
                        },

                        _ => {
                            // log::error!("Ignoring ...");
                        }
                    }
                }

                Err(err) => {
                    log::error!("Error: {}", err);
                }
            }
        }

        // The second poll we do processes packets in the receive queue from the first poll.
        interface
            .device()
            .set_poll_mode(VirtualDevicePollMode::ProcessQueuedPackets);

        match interface.poll(timestamp) {
            Ok(_) => {}
            Err(error) => {
                debug!("Poll error: {}", error)
            }
        }

        // Handle DNS.
        dns_manager.process_sockets(&mut interface);

        // Say hello to TCP connections and close them.
        for value in interface.sockets_mut() {
            match value {
                (_handle, Socket::Tcp(ref mut tcp_socket)) => {
                    if tcp_socket.may_send() {
                        tcp_socket
                            .send_slice(&"Hello from onionmasq!\n".as_bytes())
                            .unwrap();
                        tcp_socket.close();
                    }
                }

                _ => continue,
            }
        }

        // Wait for next iteration.
        phy_wait(device_fd, interface.poll_delay(timestamp)).expect("wait error");
    }
}
